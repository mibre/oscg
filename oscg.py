from os import environ
import openstack
from pathlib import Path
import logging

logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)

file_patch = str(Path.home()) + "/.ssh/config"

openstack.enable_logging(debug=False)


class Server:
    def __init__(self, server_config):
        self.ipv4_fixed = None
        self.name = server_config['name']
        self.key_name = server_config['key_name']
        for network, details in server_config['addresses'].items():
            for ip in details:
                if ip['OS-EXT-IPS:type'] == 'fixed' and ip['version'] == 4 and not self.ipv4_fixed:
                    self.ipv4_fixed = ip['addr']


class Settings:
    project_name = environ['OS_PROJECT_NAME']


class SshConfig:
    begin_config = f"##generated by oscg for project: {Settings.project_name}\n"
    end_config = f"##end config for project: {Settings.project_name}\n"

    def save_servers(self, servers):
        with open(file_patch, "a+") as config_file:
            config_file.write(self.begin_config)
            for server in servers:
                config_file.write(f"Host {server.name} \n")
                config_file.write(f"\tHostName {server.ipv4_fixed} \n")
                config_file.write(f"\tUser root \n")
                config_file.write(f"\tIdentityFile ~/.ssh/{server.key_name} \n")
            config_file.write(self.end_config)

    def remove_old_config(self):
        with open(file_patch, "r+") as config_file:
            old_config = config_file.readlines()
            beg_index = False
            end_index = False
            try:
                beg_index = old_config.index(self.begin_config)
            except ValueError:
                pass
            try:
                end_index = old_config.index(self.end_config)
            except ValueError:
                if not beg_index:
                    logger.info("old config not found, nothing todo")
                    return
                else:
                    exit("missing end config")
            if not beg_index and end_index:
                exit("found end config but not found beginning")

            if beg_index and end_index:
                new_config = old_config[:beg_index] + old_config[end_index + 1:]
            config_file.seek(0)
            config_file.truncate()
            config_file.writelines(new_config)


if __name__ == "__main__":
    conn = openstack.connect()

    servers = []
    for server in conn.compute.servers():
        servers.append(Server(server.to_dict()))
    SshConfig().remove_old_config()
    SshConfig().save_servers(servers)
